//
//  YRollerButton.m
//  YRoller
//
//  Created by Michael Foget on 10/20/13.
//  Copyright (c) 2013 Michael Foget. All rights reserved.
//

#import "YRollerButton.h"

@interface YRollerButton ()

@property CGRect portraitFrame;
@property UIColor *enabledColor;

@end

@implementation YRollerButton

- (void)awakeFromNib
{
    // This is the old init; runs once when application loads for objects
    // in the storyboard
}

- (void)initializeRollerButtonValues
{
    self.buttonEnabled = true;
    self.enabledColor = self.backgroundColor;
    self.rollValue = 0;
    self.portraitFrame = self.frame;
}

- (void)setFrameInLandscapeBetweenLeftFrame:(CGRect)leftFrame
                               rightFrame:(CGRect)rightFrame
{
    CGRect landscapeFrame = self.frame;
    landscapeFrame.origin.x =
            (((rightFrame.origin.x - (leftFrame.origin.x + 50)) - 50) / 2)
                    + (leftFrame.origin.x + 50);
    landscapeFrame.origin.y = 30;
    self.frame = landscapeFrame;
}

- (void)setFrameInPotrait
{
    self.frame = self.portraitFrame;
}

- (void)toggleButtonEnabled
{
    if (self.buttonEnabled) {
        
        [self disenableButton];
        
    } else {
        
        [self enableButton];
    }
}

- (void)enableButton
{
    self.buttonEnabled = true;
    self.backgroundColor = self.enabledColor;
}

- (void)disenableButton
{
    self.buttonEnabled = false;
    self.backgroundColor = [UIColor lightGrayColor];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
