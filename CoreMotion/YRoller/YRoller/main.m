//
//  main.m
//  YRoller
//
//  Created by Michael Foget on 10/14/13.
//  Copyright (c) 2013 Michael Foget. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "YRollerAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([YRollerAppDelegate class]));
    }
}
