//
//  YRollerViewController.m
//  YRoller
//
//  Created by Michael Foget on 10/14/13.
//  Copyright (c) 2013 Michael Foget. All rights reserved.
//

#import "YRollerViewController.h"
#import "YRollerButton.h"

@interface YRollerViewController ()

@property (weak, nonatomic) IBOutlet YRollerButton *dice1;
@property (weak, nonatomic) IBOutlet YRollerButton *dice2;
@property (weak, nonatomic) IBOutlet YRollerButton *dice3;
@property (weak, nonatomic) IBOutlet YRollerButton *dice4;
@property (weak, nonatomic) IBOutlet YRollerButton *dice5;
@property (weak, nonatomic) IBOutlet UITextField *rollTotalPoints;
@property (weak, nonatomic) IBOutlet UITextField *numberOfRolls;

@property int rollCounter;

@property NSArray *diceArray;

@end

@implementation YRollerViewController

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.diceArray =
            @[self.dice1, self.dice2, self.dice3, self.dice4, self.dice5];
    
    for (YRollerButton *currentButton in self.diceArray) {
        
        [currentButton initializeRollerButtonValues];
    }
    
    self.rollCounter = 0;

    // Request to start receiving accelerometer events and turn on
    // accelerometer; designate what to do with notification.

    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    
    [[NSNotificationCenter defaultCenter]
             addObserver:self
                selector:@selector(orientationChanged:)
                    name:UIDeviceOrientationDidChangeNotification
                  object:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    [self becomeFirstResponder];
}

-(void) viewDidDisappear
{
    // Request to stop receiving accelerometer events
    // and turn off accelerometer
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[UIDevice currentDevice] endGeneratingDeviceOrientationNotifications];
}

- (BOOL)canBecomeFirstResponder
{
    return YES;
}

- (void)motionEnded:(UIEventSubtype)motion withEvent:(UIEvent *)event
{
    if (motion == UIEventSubtypeMotionShake) {
        
        [self rollTheDice];
    }
}

- (void)orientationChanged:(NSNotification *)notification
{
    // Respond to changes in device orientation
    
    UILocalNotification *localNotification = [UILocalNotification new];
    
    if ([[UIDevice currentDevice] orientation] == UIDeviceOrientationFaceDown) {
        
        NSLog(@"Device Face Down!");
        
        localNotification.alertBody = @"Device Face Down!";

    } else if ([[UIDevice currentDevice] orientation] ==
                                    UIDeviceOrientationFaceUp) {
        
        NSLog(@"Device Face Up!");
        
        localNotification.alertBody = @"Device Face Up!";
        
    } else if ([[UIDevice currentDevice] orientation] ==
              UIDeviceOrientationPortraitUpsideDown) {
        
        NSLog(@"Device Upside Down!");

        localNotification.alertBody = @"Device Upside Down!";
    }

    [[UIApplication sharedApplication]
            presentLocalNotificationNow:localNotification];
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

- (void)willAnimateRotationToInterfaceOrientation:
                (UIInterfaceOrientation)toInterfaceOrientation
       duration:(NSTimeInterval)duration
{
    NSLog(@"Rotating");
    if (UIInterfaceOrientationIsPortrait(toInterfaceOrientation)) {
    
        [self moveToPortraitOrientation];
        
    } else {
        
        [self moveToLandscapeOrientation];
    }
}

- (void)moveToLandscapeOrientation
{
    [UIView setAnimationDuration:0.5];
    [self.dice2 setFrameInLandscapeBetweenLeftFrame:self.dice1.frame
                                       rightFrame:self.dice3.frame];
    [self.dice4 setFrameInLandscapeBetweenLeftFrame:self.dice3.frame
                                       rightFrame:self.dice5.frame];
}

- (void)moveToPortraitOrientation
{
    [UIView setAnimationDuration:0.5];
    [self.dice2 setFrameInPotrait];
    [self.dice4 setFrameInPotrait];
}

- (IBAction)diceClicked:(YRollerButton *)sender
{
    [sender toggleButtonEnabled];
}

- (IBAction)rollTheDice
{
    if (self.rollCounter == 3) {
        
        [self resetYRoller];
    }
    
    int rollTotal = 0;
    for (YRollerButton *currentButton in self.diceArray) {
        
        if (currentButton.buttonEnabled) {
            
            currentButton.rollValue = [self randomNumberFrom:0 to:6];
            [currentButton
                    setTitle:[NSString stringWithFormat:@"%d",
                                    currentButton.rollValue]
                    forState:UIControlStateNormal];
        }
        
        rollTotal += currentButton.rollValue;
    }
    
    self.rollTotalPoints.text = [NSString stringWithFormat:@"%d", rollTotal];
    self.rollCounter++;
    self.numberOfRolls.text =
            [NSString stringWithFormat:@"%d", self.rollCounter];
}

- (int)randomNumberFrom:(int)first to:(int)second
{
    int difference = second - first;
    
    int randomValue = (arc4random() % difference) + 1;
    
    return randomValue + first;
}

- (void)resetYRoller
{
    for (YRollerButton *currentButton in self.diceArray) {

        [currentButton enableButton];
        self.rollCounter = 0;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
