//
//  YRollerButton.h
//  YRoller
//
//  Created by Michael Foget on 10/20/13.
//  Copyright (c) 2013 Michael Foget. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YRollerButton : UIButton

@property BOOL buttonEnabled;
@property int rollValue;

- (void)initializeRollerButtonValues;
- (void)setFrameInLandscapeBetweenLeftFrame:(CGRect)leftFrame
                               rightFrame:(CGRect)rightFrame;
- (void)setFrameInPotrait;
- (void)toggleButtonEnabled;
- (void)enableButton;

@end
