//
//  YRollerAppDelegate.h
//  YRoller
//
//  Created by Michael Foget on 10/14/13.
//  Copyright (c) 2013 Michael Foget. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YRollerAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
