//
//  MBFAppDelegate.h
//  PlayerMovement
//
//  Created by Michael Foget on 4/7/14.
//  Copyright (c) 2014 Michael Foget. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreMotion/CoreMotion.h>

@class MBFMotionViewController;

@interface MBFAppDelegate : UIResponder <UIApplicationDelegate> {
    
    CMMotionManager *motionManager;
    
}

@property (strong, nonatomic) UIWindow *window;

@property (readonly) CMMotionManager *motionManager;

@property (strong, nonatomic) MBFMotionViewController *viewController;

@end
