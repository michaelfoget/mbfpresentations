<?php

// Configuration file for push.php

$config = array(
	// These are the settings for development mode
	'development' => array(

		// The APNS server that we will use
		'server' => 'gateway.sandbox.push.apple.com:2195',

		// The SSL certificate that allows us to connect to the APNS servers
		'certificate' => '/** Your certificate directory path **/promoTestDevelopment.pem',
		'passphrase' => 'promotest',

		// Configuration of the MySQL database
		'db' => array(
			'host'     => 'localhost',
			'dbname'   => 'promos',
			'username' => 'username',
			'password' => 'password',
			),

		// Name and path of our log file
		'logfile' => '/** Your log file directory path **/push_development.log',
		),

	// These are the settings for production mode - NOT USED IN DEMO
	'production' => array(

		// The APNS server that we will use
		'server' => 'gateway.push.apple.com:2195',

		// The SSL certificate that allows us to connect to the APNS servers
		'certificate' => 'ck_production.pem',
		'passphrase' => 'pushchat',

		// Configuration of the MySQL database
		'db' => array(
			'host'     => 'localhost',
			'dbname'   => 'pushchat',
			'username' => 'pushchat',
			'password' => 'password',
			),

		// Name and path of our log file
		'logfile' => '../log/push_production.log',
		),
	);
