<?php

// Run PromoTestWatcher on your external device (will not work on simulator);
// look on Console for your device token.
// Put your device token here (without spaces):

$deviceToken = '** Your deviceToken **';

// Put your certificate pem file pathname here:

$localcert = '/** Your certificate directory path **/promoTestDevelopment.pem';

// Put your private key's passphrase here:

$passphrase = 'promotest';

// Put your alert message here:

$message = 'My first push notification!';

////////////////////////////////////////////////////////////////////////////////
// echo $message;
$ctx = stream_context_create();
stream_context_set_option($ctx, 'ssl', 'local_cert', $localcert);
stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

// Open a connection to the APNS server
$fp = stream_socket_client(

/*
	This original line of code caused error.  -  MBF 03/25/14

	'ssl://gateway.sandbox.push.apple.com:2195', $err,

*/
	
	'sslv3://gateway.sandbox.push.apple.com:2195', $err,
	$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

if (!$fp)
	exit("Failed to connect: $err $errstr" . PHP_EOL);

echo 'Connected to APNS' . PHP_EOL;

// Create the payload body
$body['aps'] = array(
	'alert' => $message,
	'badge' => 13,
	'sound' => 'default'
	);

// Encode the payload as JSON
$payload = json_encode($body);

// Build the binary notification
$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

// Send it to the server
$result = fwrite($fp, $msg, strlen($msg));

if (!$result)
	echo 'Message not delivered' . PHP_EOL;
else
	echo 'Message successfully delivered' . PHP_EOL;

// Close the connection to the server
fclose($fp);
