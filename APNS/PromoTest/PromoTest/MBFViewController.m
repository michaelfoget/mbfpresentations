//
//  MBFViewController.m
//  PromoTest
//
//  Created by Michael Foget on 3/18/14.
//  Copyright (c) 2014 Michael Foget. All rights reserved.
//

#import "MBFViewController.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "SBJSON.h"
#import "MBProgressHUD.h"

@interface MBFViewController ()

@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) NSString *codeRedeemed;

@end

@implementation MBFViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSLog(@"Want to redeem: %@", textField.text);
    
    // Get device unique ID
    UIDevice *device = [UIDevice currentDevice];
//    NSString *uniqueIdentifier = [device uniqueIdentifier];  Commented out by MBF - depreciated
    NSUUID *identifierUUID = [device identifierForVendor];  //  Changed by MBF - preplacement for uniqueIdentifier
//    NSString *uniqueIdentifier =
//            [NSString stringWithFormat:@"%@", identifierUUID];  //  Added/commeted out by MBF - too unique!
    NSString *uniqueIdentifier = identifierUUID.UUIDString;  //  changed to by MBF
    
    // Start request
//    NSString *code = textField.text;
    self.codeRedeemed = textField.text;
//    NSURL *url = [NSURL URLWithString:@"http://*your host IP address here*/promos/"];
    NSURL *url = [NSURL URLWithString:@"http://10.97.27.74/promos/"];
    ASIFormDataRequest *request = [ASIFormDataRequest requestWithURL:url];
    
    [request setPostValue:@"1" forKey:@"rw_app_id"];
    [request setPostValue:self.codeRedeemed forKey:@"code"];
    [request setPostValue:uniqueIdentifier forKey:@"device_id"];
    [request setDelegate:self];
    [request startAsynchronous];
    
    // Hide keyword
    [textField resignFirstResponder];
    
    // Clear text field
//    textView.text = @"";      //  Commented out by MBF
//    self.textView.text = @"";      //  Commented out by MBF - wasn't clearing text field
    textField.text = @"";       //  Added by MBF to clear text

    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.labelText = @"Redeeming code...";

    return TRUE;
}

- (void)requestFinished:(ASIHTTPRequest *)request
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSLog(@"code = %d", request.responseStatusCode);
    NSLog(@"result = %@", request.responseString);
    if (request.responseStatusCode == 400) {
//        textView.text = @"Invalid code";
        self.textView.text = @"Invalid code";
    } else if (request.responseStatusCode == 403) {
//        textView.text = @"Code already used";
        self.textView.text = [NSString stringWithFormat:@"Promo code - %@ - already used; Try another code!", self.codeRedeemed];
    } else if (request.responseStatusCode == 200) {
        NSString *responseString = [request responseString];
        NSDictionary *responseDict = [responseString JSONValue];
        NSString *unlockCode = [responseDict objectForKey:@"unlock_code"];
        
        if ([unlockCode compare:@"com.razeware.test.unlock.cake"] == NSOrderedSame) {
//            textView.text = @"The cake is a lie!";
            self.textView.text = [NSString stringWithFormat:@"You have unlocked promo code - %@; Enjoy!", self.codeRedeemed];
        } else {
//            textView.text = [NSString stringWithFormat:@"Received unexpected unlock code: %@", unlockCode];
            self.textView.text = [NSString stringWithFormat:@"Received unexpected unlock code: %@", unlockCode];
        }
        
    } else {
//        textView.text = @"Unexpected error";
        self.textView.text = [NSString stringWithFormat:@"%@ - %@",
                              @"Unexpected error", request.responseStatusMessage];
    }
    
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSError *error = [request error];
//    textView.text = error.localizedDescription;
    self.textView.text = [NSString stringWithFormat:@"%@ - %ld",
                                error.localizedDescription, (long)error.code];
}

@end
