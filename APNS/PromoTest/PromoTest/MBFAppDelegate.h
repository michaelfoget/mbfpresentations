//
//  MBFAppDelegate.h
//  PromoTest
//
//  Created by Michael Foget on 3/18/14.
//  Copyright (c) 2014 Michael Foget. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MBFAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
