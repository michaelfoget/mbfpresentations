//
//  main.m
//  PromoTestWatcher
//
//  Created by Michael Foget on 3/24/14.
//  Copyright (c) 2014 Michael Foget. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MBFAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MBFAppDelegate class]));
    }
}
