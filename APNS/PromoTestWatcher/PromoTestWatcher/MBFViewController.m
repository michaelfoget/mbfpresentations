//
//  MBFViewController.m
//  PromoTestWatcher
//
//  Created by Michael Foget on 3/24/14.
//  Copyright (c) 2014 Michael Foget. All rights reserved.
//

#import "MBFViewController.h"

@interface MBFViewController ()

@end

@implementation MBFViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
