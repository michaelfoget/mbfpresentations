//
//  MBFAppDelegate.m
//  PromoTestWatcher
//
//  Created by Michael Foget on 3/24/14.
//  Copyright (c) 2014 Michael Foget. All rights reserved.
//

#import "MBFAppDelegate.h"

@implementation MBFAppDelegate

- (BOOL)application:(UIApplication *)application
    didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //    Tell the device that this app wants to recieve notifications.

    [[UIApplication sharedApplication]
        registerForRemoteNotificationTypes:(
                                            UIRemoteNotificationTypeBadge |
                                            UIRemoteNotificationTypeAlert |
                                            UIRemoteNotificationTypeSound)];
    
    //    If app wasn’t running when the notification came in, it is launched
    //    and the notification is passed as part of the launchOptions
    //    dictionary. Add code here to handle notification payload.
    
    return YES;
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)application:(UIApplication*)application
    didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    //	This method is invoked when the app registers with APNS for remote
    //  notifications. This is the "address" to which notifications will be
    //  sent by the service to this device running the app.

	NSLog(@"My token is: %@", deviceToken);
}

- (void)application:(UIApplication*)application
    didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    //	This method is invoked when the app registers with APNS for remote
    //  notifications and receives an error.

	NSLog(@"Failed to get token, error: %@", error);
}

- (void)application:(UIApplication *)application
    didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    //	This method is invoked if the app is active when a notification comes
    //  in. On iOS 4.0 or later, if the app was suspended in the background it
    //  is woken up and this method is also called. Use UIApplication’s
    //  applicationState property to find out whether the app was suspended or
    //  not. Add code here to handle notification payload.
}
@end
