DROP TABLE IF EXISTS rw_promo_code;
DROP TABLE IF EXISTS rw_app;
DROP TABLE IF EXISTS rw_promo_code_redeemed;

CREATE TABLE rw_promo_code (
    id mediumint NOT NULL AUTO_INCREMENT PRIMARY KEY,	
    rw_app_id tinyint NOT NULL, 
    code varchar(255) NOT NULL,
    unlock_code varchar(255) NOT NULL,
    uses_remaining smallint NOT NULL
);

CREATE TABLE rw_app (
    id mediumint NOT NULL AUTO_INCREMENT PRIMARY KEY,	
    app_id varchar(255) NOT NULL
);

CREATE TABLE rw_promo_code_redeemed (
    id mediumint NOT NULL AUTO_INCREMENT PRIMARY KEY,	
    rw_promo_code_id mediumint NOT NULL,
    device_id varchar(255) NOT NULL,
    redeemed_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    time_push_sent TIMESTAMP NULL
);

INSERT INTO rw_app VALUES(1, 'com.razeware.test');
INSERT INTO rw_promo_code VALUES(1, 1, 'test1', 'com.razeware.test.unlock.cake', 10000);
INSERT INTO rw_promo_code VALUES(2, 1, 'test2', 'com.razeware.test.unlock.cake', 10000);
INSERT INTO rw_promo_code VALUES(3, 1, 'test3', 'com.razeware.test.unlock.cake', 10000);
INSERT INTO rw_promo_code VALUES(4, 1, 'test4', 'com.razeware.test.unlock.cake', 10000);
INSERT INTO rw_promo_code VALUES(5, 1, 'test5', 'com.razeware.test.unlock.cake', 10000);
INSERT INTO rw_promo_code VALUES(6, 1, 'test6', 'com.razeware.test.unlock.cake', 10000);
INSERT INTO rw_promo_code VALUES(7, 1, 'test7', 'com.razeware.test.unlock.cake', 10000);
INSERT INTO rw_promo_code VALUES(8, 1, 'test8', 'com.razeware.test.unlock.cake', 10000);
INSERT INTO rw_promo_code VALUES(9, 1, 'test9', 'com.razeware.test.unlock.cake', 10000);
INSERT INTO rw_promo_code VALUES(10, 1, 'test10', 'com.razeware.test.unlock.cake', 10000);
