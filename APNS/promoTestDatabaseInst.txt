/*

	These are the command line instructions for creating the Promos database
	for the PromoTest application.
*/

//  Connect to mysql as root

$ mysql -u root -p
Enter password: 
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 1286
Server version: 5.1.37-1ubuntu5.1-log (Ubuntu)

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

//  Create promos database

mysql> create database promos;
Query OK, 1 row affected (0.00 sec)

//  Grant privileges to user with password (change username & password 
//  if you wish but remember to propagate to app)

mysql> use promos;
Database changed
mysql> grant all privileges on promos.* to 'username'@'localhost' identified by 'password';
Query OK, 0 rows affected (0.00 sec)

//  Quit mysql connection

mysql> exit
Bye

//  Connect to mysql promos database as newly created user and execute sql
//  to build and populated tables (you need to be in the directory where the sql
//  is located or provide a fully qualified pathname).

$ mysql -u username -p promos < promoTest.sql
Enter password: 

//  Connect to mysql as root

$ mysql -u root -p
Enter password: 
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 1417
Server version: 5.1.37-1ubuntu5.1-log (Ubuntu)

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

//  Connect to promos database

mysql> use promos;
Database changed

//  Verify that tables were created and records inserted

mysql> show tables ;
+------------------------+
| Tables_in_promos       |
+------------------------+
| rw_app                 | 
| rw_promo_code          | 
| rw_promo_code_redeemed | 
+------------------------+
3 rows in set (0.00 sec)

mysql> select * from rw_app;
+----+-------------------+
| id | app_id            |
+----+-------------------+
|  1 | com.razeware.test |
+----+-------------------+
1 row in set (0.00 sec)

mysql> select * from rw_promo_code;
+----+-----------+--------+-------------------------------+----------------+
| id | rw_app_id | code   | unlock_code                   | uses_remaining |
+----+-----------+--------+-------------------------------+----------------+
|  1 |         1 | test1  | com.razeware.test.unlock.cake |          10000 |
|  2 |         1 | test2  | com.razeware.test.unlock.cake |          10000 |
|  3 |         1 | test3  | com.razeware.test.unlock.cake |          10000 |
|  4 |         1 | test4  | com.razeware.test.unlock.cake |          10000 |
|  5 |         1 | test5  | com.razeware.test.unlock.cake |          10000 |
|  6 |         1 | test6  | com.razeware.test.unlock.cake |          10000 |
|  7 |         1 | test7  | com.razeware.test.unlock.cake |          10000 |
|  8 |         1 | test8  | com.razeware.test.unlock.cake |          10000 |
|  9 |         1 | test9  | com.razeware.test.unlock.cake |          10000 |
| 10 |         1 | test10 | com.razeware.test.unlock.cake |          10000 |
+----+-----------+--------+-------------------------------+----------------+
10 rows in set (0.00 sec)

mysql> select * from rw_promo_code_redeemed;
Empty set (0.00 sec)




